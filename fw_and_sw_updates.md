# WaveDAQ Firmware and Software Updates

## Maintainers
Elmar Schmid [elmar.schmid@psi.ch]

## Authors
Elmar Schmid [elmar.schmid@psi.ch]

# File locations

Subfolders /prod always contain the productive system currently in operation while other subfolders are used for special setups and tests.
## DCB

### File System

- **/boot/prod/**
  - *BOOT.bin*
    Containts PL Firmware, FSBL and U-Boot (reverence to copy to SD Card)
  - *uImage*
    Kernel Image
  - *zynq_psi_wavedaq_dcb.dtb*
    Linux Device Tree Blob
- **Rootfs**: root file system for Linux
- **/fw_sw/...**
  - **...dcb/prod/**
    - *dcbs*
      DCB server software
    - *wds*
      WaveDream server software
    - *dma_packet_scheduler_v1_0.ko*
      Kernel module driver for DMA packet scheduler (will be integrated in Kernel)
    - *html/*
      Contains files and scripts for WDS web application
  - **...tcb/prod/type_1 ,2 ,3**
    - *TCB_TOP.bit*
      Firmware for the corresponding TCB type
  - **...wdb/prod/rev_f ,g**
    - *download.bit*
      firmware for the corresponding WDB revision
    - *app_sys_ctrl.srec*
      software for corresponding WDB revision

### SD Card

Except for BOOT.bin, the files on the SD card are only used if the system

- **Partition 1** (vfat, mandatory)
  - *BOOT.bin*
    Containts PL Firmware, FSBL and U-Boot. Mandatory in this place for system boot.
  - *uImage*
    Kernel Image. Optional in NFS boot configuration, mandatory to boot from SD card.
- **Partition 2** (ext4, optional)
  - *Rootfs*
    Root file system for Linux. Might also be mounted via NFS.
- **Partition 3** (ext4, optional)
  - */home* folders
    User home folders. Might also be mounted via NFS.
- **Partition 4** (ext4, optional)
  - */fw_sw* folder
    contains firmware and software files (see above). Might also be mounted via NFS.

# Software Build Instructions

To be updated and completed!!!

## DCB Server: dcbs

### To compile dcbs
The dcbs server can be compiled and installed from the wavedaq_sw repository (submodule in /sw):

    $ cd wavedaq_main/sw/dcb/app/build
    $ cmake ..
    $ sudo make install

### Configure dcbs for automatic start after boot

The dcbs startup script in /etc/init.d has to be added to the start configuration:

    $ sudo chkconfig --add dcbs.sh
    $ sudo chkconfig --level 345 dcbs.sh on
    $ sudo /etc/init.d/dcbs.sh start

### Connection to dcbs

The connection to dcbs via UDP can be made with netcat:

    $ nc -u dcbxx 3000
    <return>
    dcbxx> help
    dcbxx> scan
