# General Information

## Maintainers
Stefan Ritt [stefan.ritt@psi.ch]
Elmar Schmid [elmar.schmid@psi.ch]

## Authors
Marco Francesconi [marco.francesconi@pi.infn.it]
Luca Galli [luca.galli@pi.infn.it]
Stefan Ritt [stefan.ritt@psi.ch]
Elmar Schmid [elmar.schmid@psi.ch]

# Description
This repositry contains submodules for hardware, firmware, software and documentation of WaveDAQ.

# Documentation
The following folders contain overall documentation as well as documentation for specific parts of the system:

1. [Overall System Documentation](doc/)
2. [CMB Firmware Submodule Readme](fw/cmb/README.md)
3. [DCB Firmware](fw/dcb/doc/)
   1. [Vivado Ip DCB Register Bank](fw/dcb/dcb_vivado_hw/ip_repo/axi_dcb_register_bank_1.0/doc/dcb_register_map.html)
   2. [Vivado Ip DMA Packet Scheduler (PDF)](fw/dcb/dcb_vivado_hw/ip_repo/dma_packet_scheduler_v1_0/doc/dma_packet_scheduler.pdf)
   3. [Vivado Ip SERDES Packet Receiver (PDF)](fw/dcb/dcb_vivado_hw/ip_repo/serdes_packet_receiver_v1_0/doc/serdes_packet_receiver_v1_0.pdf)
4. [TCB Firmware](fw/dcb/README.md)
5. [WDB Firmware](fw/wdb/doc/)
6. [Hardware Submodule Readme](hw/README.md)
7. [Linux Yocto Environment](linux/yocto_build_env)
8. [Software](sw/doc/)
   1. [DCB Register Bank Contents](sw/doc/dcb_register_map.html)
   2. [WDB Register Bank Contents v8 (Rev. E&F)](sw/doc/wd2_register_map_v8.html)
   3. [WDB Register Bank Contents v9 (Rev. G)](sw/doc/wd2_register_map_v9.html)

# Repository Operation

## Branch Concept
This repository represents a collection of repositories belonging to the MEGII project. I.e. it only contains references to other repositories which are included as submodules. It does not contain any source code or documentation itself.

Thus the branch concept is a little different from a source repository. There are only two branches, a master and a develop branch.

==**Caution: Due to the special usage of this repository, master and develop branch must never be merged!**==

### Master Branch
The master branch contains references to the latest master commits of the submodules master branches.
The corresponding submodule commits reflect a stable, consistant state i.e. hardware, firmware and software are all compatible to each other.

### Develop Branch
The develop branch contains references to the latest commits of the submodules develop branches.
Note that the corresponding submodule might not reflect a stable, consistent state i.e. there can be inconsistencies between hardware, firmware and software.

## How to clone only part of the submodules
Some sobmodule repositories are rather large so you might not want to clone all of them. There are two ways of cloning only a subset of the submodules:

### Initializing only specific submodules
One way to clone only some of the submodles is by simply initializing only the submodules that are really needed before calling `git submodule update --recursive`. If submodules were automatically added when the main repository was cloned using the `--recurse-submodules` option see how to deinit submodules in the next subsection.

The available submodule paths are:
* doc
* fw/cmb
* fw/dcb
* fw/tcb
* fw/wdb
* hw
* linux/yocto_build_env
* linux/yocto_layer
* sw

For all submodules you want to load, do
```
git submodule init -- <submodule-path>
```

Don't forget to update after all submodules are initialized
```
git submodule update --recursive.
```

If you want to add and update a submodule containing more submodules you can do it in one step by calling
```
git submodule update --init --recursive -- <submodule-path>
```

### Deinitialize specific submodules
If specific submodules are already loaded and updated but you don't want them in your working directory, you can remove them using the `deinit` command.

To remove a specific submodule from the working directory, do
```
git submodule deinit -- <submodule-path>
```
